; #FUNCTION# ====================================================================================================================
; Name ..........: MBR Bot
; Description ...: This file contens the Sequence that runs all MBR Bot
; Author ........:  (2014)
; Modified ......:
; Remarks .......: This file is part of MyBot, previously known as ClashGameBot. Copyright 2015-2016
;                  MyBot is distributed under the terms of the GNU GPL
; Related .......:
; Link ..........: https://github.com/MyBotRun/MyBot/wiki
; Example .......: No
; ===============================================================================================================================

#RequireAdmin
#AutoIt3Wrapper_UseX64=7n
#AutoIt3Wrapper_Run_Au3Stripper=y
#Au3Stripper_Parameters=/mo /rsln
;#AutoIt3Wrapper_Change2CUI=y
;#pragma compile(Console, true)
#pragma compile(Icon, "Images\MyBot.ico")
#pragma compile(FileDescription, Clash of Clans Bot - A Free Clash of Clans bot - https://mybot.run)
#pragma compile(ProductName, My Bot)
#pragma compile(ProductVersion, 6.3)
#pragma compile(FileVersion, 6.3)
#pragma compile(LegalCopyright, © https://mybot.run)
#pragma compile(Out, MyBot.run.exe)  ; Required

#include <WindowsConstants.au3>
#include <WinAPI.au3>
#include <Process.au3>

;~ Boost launch time by increasing process priority (will be restored again when finished launching)
Local $iBotProcessPriority = _ProcessGetPriority(@AutoItPID)
ProcessSetPriority(@AutoItPID, $PROCESS_ABOVENORMAL)

Global $iBotLaunchTime = 0
Local $hBotLaunchTime = TimerInit()

$sBotVersion = "v6.3.u5" ;~ Don't add more here, but below. Version can't be longer than vX.y.z because it it also use on Checkversion()
$sBotTitle = "My Bot " & $sBotVersion & " " ;~ Don't use any non file name supported characters like \ / : * ? " < > |

#include "COCBot\functions\Config\DelayTimes.au3"
#include "COCBot\MBR Global Variables.au3"
_GDIPlus_Startup()
#include "COCBot\GUI\MBR GUI Design Splash.au3"
#include "COCBot\functions\Config\ScreenCoordinates.au3"
#include "COCBot\functions\Other\ExtMsgBox.au3"

Opt("GUIResizeMode", $GUI_DOCKALL) ; Default resize mode for dock android support
Opt("GUIEventOptions", 1) ; Handle minimize and restore for dock android support
Opt("GUICloseOnESC", 0); Don't send the $GUI_EVENT_CLOSE message when ESC is pressed.
Opt("WinTitleMatchMode", 3) ; Window Title exact match mode

If Not FileExists(@ScriptDir & "\License.txt") Then
	$license = InetGet("http://www.gnu.org/licenses/gpl-3.0.txt", @ScriptDir & "\License.txt")
EndIf

;multilanguage
#include "COCBot\functions\Other\Multilanguage.au3"
DetectLanguage()
Local $sMsg

$sMsg = GetTranslated(500, 1, "Don't Run/Compile the Script as (x64)! Try to Run/Compile the Script as (x86) to get the bot to work.\r\n" & _
							  "If this message still appears, try to re-install AutoIt.")
If @AutoItX64 = 1 Then
	If IsHWnd($hSplash) Then GUIDelete($hSplash) ; Delete the splash screen since we don't need it anymore
	MsgBox(0, "", $sMsg)
	_GDIPlus_Shutdown()
	Exit
EndIf

#include "COCBot\functions\Other\MBRFunc.au3"
; check for VC2010, .NET software and MyBot Files and Folders
If CheckPrerequisites(True) Then
	MBRFunc(True) ; start MBRFunctions dll
EndIf

#include "COCBot\functions\Android\Android.au3"

; Update Bot title
$sBotTitle = $sBotTitle & "(" & ($AndroidInstance <> "" ? $AndroidInstance : $Android) & ")" ;Do not change this. If you do, multiple instances will not work.

UpdateSplashTitle($sBotTitle & GetTranslated(500, 20, ", Profile: %s", $sCurrProfile))

If $bBotLaunchOption_Restart = True Then
   If WinGetHandle($sBotTitle) Then SplashStep(GetTranslated(500, 36, "Closing previous bot..."))
   If CloseRunningBot($sBotTitle) = True Then
	  ; wait for Mutexes to get disposed
	  Sleep(3000)
   EndIf
Else
	SplashStep("")
EndIF

Local $cmdLineHelp = GetTranslated(500, 2, "By using the commandline (or a shortcut) you can start multiple Bots:\r\n" & _
					 "     MyBot.run.exe [ProfileName] [EmulatorName] [InstanceName]\r\n\r\n" & _
					 "With the first command line parameter, specify the Profilename (you can create profiles on the Misc tab, if a " & _
					 "profilename contains a {space}, then enclose the profilename in double quotes). " & _
					 "With the second, specify the name of the Emulator and with the third, an Android Instance (not for BlueStacks). \r\n" & _
					 "Supported Emulators are MEmu, Droid4X, Nox, BlueStacks2 and BlueStacks.\r\n\r\n" & _
					 "Examples:\r\n" & _
					 "     MyBot.run.exe MyVillage BlueStacks2\r\n" & _
					 "     MyBot.run.exe ""My Second Village"" MEmu MEmu_1")

$hMutex_BotTitle = _Singleton($sBotTitle, 1)
Local $sAndroidInfo = GetTranslated(500, 3, "%s", $Android)
Local $sAndroidInfo2 = GetTranslated(500, 4, "%s (instance %s)", $Android, $AndroidInstance)
If $AndroidInstance <> "" Then
	$sAndroidInfo = $sAndroidInfo2
EndIf

$sMsg = GetTranslated(500, 5, "My Bot for %s is already running.\r\n\r\n", $sAndroidInfo)
If $hMutex_BotTitle = 0 Then
	If IsHWnd($hSplash) Then GUIDelete($hSplash) ; Delete the splash screen since we don't need it anymore
	MsgBox(BitOR($MB_OK, $MB_ICONINFORMATION, $MB_TOPMOST), $sBotTitle, $sMsg & $cmdLineHelp)
	_GDIPlus_Shutdown()
	Exit
EndIf

$hMutex_Profile = _Singleton(StringReplace($sProfilePath & "\" & $sCurrProfile, "\", "-"), 1)
$sMsg = GetTranslated(500, 6, "My Bot with Profile %s is already running in %s.\r\n\r\n", $sCurrProfile, $sProfilePath & "\" & $sCurrProfile)
If $hMutex_Profile = 0 Then
	_WinAPI_CloseHandle($hMutex_BotTitle)
	If IsHWnd($hSplash) Then GUIDelete($hSplash) ; Delete the splash screen since we don't need it anymore
	MsgBox(BitOR($MB_OK, $MB_ICONINFORMATION, $MB_TOPMOST), $sBotTitle, $sMsg & $cmdLineHelp)
	_GDIPlus_Shutdown()
	Exit
EndIf

$hMutex_MyBot = _Singleton("MyBot.run", 1)
$OnlyInstance = $hMutex_MyBot <> 0 ; And False
SetDebugLog("My Bot is " & ($OnlyInstance ? "" : "not ") & "the only running instance")

#include "COCBot\MBR Global Variables Troops.au3"
#include "COCBot\MBR GUI Design.au3"
#include "COCBot\MBR GUI Control.au3"
#include "COCBot\MBR Functions.au3"

CheckPrerequisites(False)
;DirCreate($sTemplates)
DirCreate($sPreset)
DirCreate($sProfilePath & "\" & $sCurrProfile)
DirCreate($dirLogs)
DirCreate($dirLoots)
DirCreate($dirTemp)
DirCreate($dirTempDebug)


$donateimagefoldercapture = $sProfilePath & "\" & $sCurrProfile & '\Donate\'
$donateimagefoldercaptureWhiteList = $sProfilePath & "\" & $sCurrProfile & '\Donate\White List\'
$donateimagefoldercaptureBlackList = $sProfilePath & "\" & $sCurrProfile & '\Donate\Black List\'
DirCreate( $donateimagefoldercapture)
DirCreate( $donateimagefoldercaptureWhiteList)
DirCreate( $donateimagefoldercaptureBlackList)


;Migrate old bot without profile support to current one
FileMove(@ScriptDir & "\*.ini", $sProfilePath & "\" & $sCurrProfile, $FC_OVERWRITE + $FC_CREATEPATH)
DirCopy(@ScriptDir & "\Logs", $sProfilePath & "\" & $sCurrProfile & "\Logs", $FC_OVERWRITE + $FC_CREATEPATH)
DirCopy(@ScriptDir & "\Loots", $sProfilePath & "\" & $sCurrProfile & "\Loots", $FC_OVERWRITE + $FC_CREATEPATH)
DirCopy(@ScriptDir & "\Temp", $sProfilePath & "\" & $sCurrProfile & "\Temp", $FC_OVERWRITE + $FC_CREATEPATH)
DirRemove(@ScriptDir & "\Logs", 1)
DirRemove(@ScriptDir & "\Loots", 1)
DirRemove(@ScriptDir & "\Temp", 1)

;Setup profile if doesn't exist yet
If FileExists($config) = 0 Then
	createProfile(True)
EndIf

If $ichkDeleteLogs = 1 Then DeleteFiles($dirLogs, "*.*", $iDeleteLogsDays, 0)
If $ichkDeleteLoots = 1 Then DeleteFiles($dirLoots, "*.*", $iDeleteLootsDays, 0)
If $ichkDeleteTemp = 1 Then DeleteFiles($dirTemp, "*.*", $iDeleteTempDays, 0)
If $ichkDeleteTemp = 1 Then DeleteFiles($dirTempDebug, "*.*", $iDeleteTempDays, 0)

$sMsg = GetTranslated(500, 7, "Found running %s %s" , $Android, $AndroidVersion)
If $FoundRunningAndroid Then
	SetLog($sMsg, $COLOR_SUCCESS)
EndIf
If $FoundInstalledAndroid Then
	SetLog("Found installed " & $Android & " " & $AndroidVersion, $COLOR_SUCCESS)
EndIf
SetLog(GetTranslated(500, 8, "Android Emulator Configuration: %s", $sAndroidInfo), $COLOR_SUCCESS)

;AdlibRegister("PushBulletRemoteControl", $PBRemoteControlInterval)
;AdlibRegister("PushBulletDeleteOldPushes", $PBDeleteOldPushesInterval)

CheckDisplay() ; verify display size and DPI (Dots Per Inch) setting

LoadTHImage() ; Load TH images
LoadElixirImage() ; Load Elixir images
LoadElixirImage75Percent(); Load Elixir images full at 75%
LoadElixirImage50Percent(); Load Elixir images full at 50%
LoadAmountOfResourcesImages()

;~ InitializeVariables();initialize variables used in extra windows
CheckVersion() ; check latest version on mybot.run site

;~ Remember time in Milliseconds bot launched
$iBotLaunchTime = TimerDiff($hBotLaunchTime)
SetDebugLog("MyBot.run launch time " & Round($iBotLaunchTime) & " ms.")

$sMsg = GetTranslated(500, 9, "Android Shield not available for %s", @OSVersion)
If $AndroidShieldEnabled = False Then
	SetLog($sMsg, $COLOR_ACTION)
EndIf

;~ Restore process priority
ProcessSetPriority(@AutoItPID, $iBotProcessPriority)

;AutoStart Bot if request
AutoStart()
While 1
	_Sleep($iDelaySleep, True, False)

	Switch $BotAction
		Case $eBotStart
			BotStart()
			If $BotAction = $eBotStart Then $BotAction = $eBotNoAction
		Case $eBotStop
			BotStop()
			If $BotAction = $eBotStop Then $BotAction = $eBotNoAction
		Case $eBotSearchMode
			BotSearchMode()
			If $BotAction = $eBotSearchMode Then $BotAction = $eBotNoAction
		Case $eBotClose
			BotClose()
	EndSwitch
WEnd

Func runBot() ;Bot that runs everything in order
	$TotalTrainedTroops = 0
	Local $Quickattack = False
	Local $iWaitTime
	While 1
		$Restart = False
		$fullArmy = False
		$CommandStop = -1
		If _Sleep($iDelayRunBot1) Then Return
		checkMainScreen()
		If $Restart = True Then ContinueLoop
		chkShieldStatus()
		If $Restart = True Then ContinueLoop

		If $quicklyfirststart = true Then
			$quicklyfirststart = False
		Else
			$Quickattack = QuickAttack()
		EndIf

	    If checkAndroidReboot() = True Then ContinueLoop
		If $Is_ClientSyncError = False And $Is_SearchLimit = False and ($Quickattack = False ) Then
	    	If BotCommand() Then btnStop()
				If _Sleep($iDelayRunBot2) Then Return
			checkMainScreen(False)
				If $Restart = True Then ContinueLoop
			If $RequestScreenshot = 1 Then PushMsg("RequestScreenshot")
			If $ichkMultyAccount = 1 Then DetectAccount() ;MULTYACCOUNT
				If _Sleep($iDelayRunBot3) Then Return
			VillageReport()
			ProfileSwitch() ; MULTYACCOUNT Switch profile
			If $OutOfGold = 1 And (Number($iGoldCurrent) >= Number($itxtRestartGold)) Then ; check if enough gold to begin searching again
				$OutOfGold = 0 ; reset out of gold flag
				Setlog("Switching back to normal after no gold to search ...", $COLOR_SUCCESS)
				$ichkBotStop = 0 ; reset halt attack variable
				$icmbBotCond = _GUICtrlComboBox_GetCurSel($cmbBotCond) ; Restore User GUI halt condition after modification for out of gold
				$bTrainEnabled = True
				$bDonationEnabled = True
				ContinueLoop ; Restart bot loop to reset $CommandStop
			EndIf
			If $OutOfElixir = 1 And (Number($iElixirCurrent) >= Number($itxtRestartElixir)) And (Number($iDarkCurrent) >= Number($itxtRestartDark)) Then ; check if enough elixir to begin searching again
				$OutOfElixir = 0 ; reset out of gold flag
				Setlog("Switching back to normal setting after no elixir to train ...", $COLOR_SUCCESS)
				$ichkBotStop = 0 ; reset halt attack variable
				$icmbBotCond = _GUICtrlComboBox_GetCurSel($cmbBotCond) ; Restore User GUI halt condition after modification for out of elixir
				$bTrainEnabled = True
				$bDonationEnabled = True
				ContinueLoop ; Restart bot loop to reset $CommandStop
			EndIf
				If _Sleep($iDelayRunBot5) Then Return
			checkMainScreen(False)
				If $Restart = True Then ContinueLoop
			Local $aRndFuncList[3] = ['Collect', 'CheckTombs', 'ReArm']
			While 1
				If $RunState = False Then Return
				If $Restart = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
				If UBound($aRndFuncList) > 1 Then
					$Index = Random(0, UBound($aRndFuncList), 1)
					If $Index > UBound($aRndFuncList) - 1 Then $Index = UBound($aRndFuncList) - 1
					_RunFunction($aRndFuncList[$Index])
					_ArrayDelete($aRndFuncList, $Index)
				Else
					_RunFunction($aRndFuncList[0])
					ExitLoop
				EndIf
			    If $Restart = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
			WEnd
			AddIdleTime()
				If $RunState = False Then Return
				If $Restart = True Then ContinueLoop
			If IsSearchAttackEnabled() Then  ; if attack is disabled skip reporting, requesting, donating, training, and boosting
			   Local $aRndFuncList[10] = ['ReplayShare', 'ReportNotify', 'DonateCC,Train', 'BoostBarracks', 'BoostSpellFactory', 'BoostDarkSpellFactory', 'BoostKing', 'BoostQueen', 'BoostWarden', 'RequestCC']
			   While 1
				   If $RunState = False Then Return
				   If $Restart = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
				   If UBound($aRndFuncList) > 1 Then
					   $Index = Random(0, UBound($aRndFuncList), 1)
					   If $Index > UBound($aRndFuncList) - 1 Then $Index = UBound($aRndFuncList) - 1
					   _RunFunction($aRndFuncList[$Index])
					   _ArrayDelete($aRndFuncList, $Index)
				   Else
					   _RunFunction($aRndFuncList[0])
					   ExitLoop
				   EndIf
				   If checkAndroidReboot() = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
			   WEnd
					If $RunState = False Then Return
					If $Restart = True Then ContinueLoop
			   If $iUnbreakableMode >= 1 Then
					If Unbreakable() = True Then ContinueLoop
				EndIf
			EndIf
			Local $aRndFuncList[3] = ['Laboratory', 'UpgradeHeroes', 'UpgradeBuilding']
			While 1
				If $RunState = False Then Return
				If $Restart = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
				If UBound($aRndFuncList) > 1 Then
					$Index = Random(0, UBound($aRndFuncList), 1)
					If $Index > UBound($aRndFuncList) - 1 Then $Index = UBound($aRndFuncList) - 1
					_RunFunction($aRndFuncList[$Index])
					_ArrayDelete($aRndFuncList, $Index)
				Else
					_RunFunction($aRndFuncList[0])
					ExitLoop
				EndIf
				If checkAndroidReboot() = True Then ContinueLoop 2 ; must be level 2 due to loop-in-loop
			WEnd
				If $RunState = False Then Return
				If $Restart = True Then ContinueLoop
			If IsSearchAttackEnabled() Then  ; If attack scheduled has attack disabled now, stop wall upgrades, and attack.
				$iNbrOfWallsUpped = 0
				UpgradeWall()
					If _Sleep($iDelayRunBot3) Then Return
					If $Restart = True Then ContinueLoop
				Idle()
					;$fullArmy1 = $fullArmy
					If _Sleep($iDelayRunBot3) Then Return
					If $Restart = True Then ContinueLoop
				SaveStatChkTownHall()
				SaveStatChkDeadBase()
				If $CommandStop <> 0 And $CommandStop <> 3 Then
				  AttackMain()
				  $SkipFirstZoomout = False
				  If $OutOfGold = 1 Then
					 Setlog("Switching to Halt Attack, Stay Online/Collect mode ...", $COLOR_ERROR)
					 $ichkBotStop = 1 ; set halt attack variable
					 $icmbBotCond = 18 ; set stay online/collect only mode
					 $FirstStart = True ; reset First time flag to ensure army balancing when returns to training
					 ContinueLoop
				  EndIf
				  If _Sleep($iDelayRunBot1) Then Return
				  If $Restart = True Then ContinueLoop
			   EndIf
			Else
				$iWaitTime = Random($iDelayWaitAttack1, $iDelayWaitAttack2)
			   SetLog("Attacking Not Planned and Skipped, Waiting random " & StringFormat("%0.1f", $iWaitTime / 1000) & " Seconds", $COLOR_WARNING)
			   If _SleepStatus($iWaitTime) Then Return False
			EndIf
	    Else ;When error occours directly goes to attack
			If $Quickattack Then
				Setlog("Quick Restart... ",$COLOR_INFO)
			Else
				If $Is_SearchLimit = True Then
					SetLog("Restarted due search limit", $COLOR_INFO)
				Else
					SetLog("Restarted after Out of Sync Error: Attack Now", $COLOR_INFO)
				EndIf
			EndIf
			If _Sleep($iDelayRunBot3) Then Return
			;  OCR read current Village Trophies when OOS restart maybe due PB or else DropTrophy skips one attack cycle after OOS
			$iTrophyCurrent = Number(getTrophyMainScreen($aTrophies[0], $aTrophies[1]))
			If $debugsetlog = 1 Then SetLog("Runbot Trophy Count: " & $iTrophyCurrent, $COLOR_DEBUG)
			AttackMain()
			$SkipFirstZoomout = False
			If $OutOfGold = 1 Then
				Setlog("Switching to Halt Attack, Stay Online/Collect mode ...", $COLOR_ERROR)
				$ichkBotStop = 1 ; set halt attack variable
				$icmbBotCond = 18 ; set stay online/collect only mode
				$FirstStart = True ; reset First time flag to ensure army balancing when returns to training
				$Is_ClientSyncError = False ; reset fast restart flag to stop OOS mode and start collecting resources
				ContinueLoop
			EndIf
			If _Sleep($iDelayRunBot5) Then Return
			If $Restart = True Then ContinueLoop
			EndIf
	  ;MultyAccount ==============================================================================================================
		If $ichkMultyAccount = 1 Then
			ActiveAccount()
		 EndIf
	WEnd
EndFunc   ;==>_runBot

Func Idle() ;Sequence that runs until Full Army
	Local $TimeIdle = 0 ;In Seconds
	If $debugsetlog = 1 Then SetLog("Func Idle ", $COLOR_DEBUG)

	While $fullArmy = False Or $bFullArmyHero = False Or $bFullArmySpells = False
		checkAndroidReboot()

		If $RequestScreenshot = 1 Then PushMsg("RequestScreenshot")
		If _Sleep($iDelayIdle1) Then Return
		If $CommandStop = -1 Then SetLog("====== Waiting for full army ======", $COLOR_SUCCESS)
		Local $hTimer = TimerInit()
		Local $iReHere = 0

	    ;If $iSkipDonateNearFulLTroopsEnable = 1 Then getArmyCapacity(true,true)
		getArmyCapacity(true,true)
		While $iReHere < 7
			$iReHere += 1
			DonateCC(True)
			If _Sleep($iDelayIdle2) Then ExitLoop
			If $Restart = True Then ExitLoop
		WEnd
		If _Sleep($iDelayIdle1) Then ExitLoop
		checkMainScreen(False) ; required here due to many possible exits
		If ($CommandStop = 3 Or $CommandStop = 0) Then
			CheckOverviewFullArmy(True, False)  ; use true parameter to open train overview window
			getArmyHeroCount(False, False)
			getArmySpellCount(False, True) ; use true parameter to close train overview window
			If _Sleep($iDelayIdle1) Then Return
			If Not ($fullArmy) And $bTrainEnabled = True Then
				SetLog("Army Camp and Barracks are not full, Training Continues...", $COLOR_ACTION)
				$CommandStop = 0
			EndIf
		EndIf
		ReplayShare($iShareAttackNow)
		If _Sleep($iDelayIdle1) Then Return
		CleanYard()
		If $Restart = True Then ExitLoop
		If $iCollectCounter > $COLLECTATCOUNT Then ; This is prevent from collecting all the time which isn't needed anyway
 			Local $aRndFuncList[2] = ['Collect', 'DonateCC']
			While 1
				If $RunState = False Then Return
				If $Restart = True Then ExitLoop
				If UBound($aRndFuncList) > 1 Then
					$Index = Random(0, UBound($aRndFuncList), 1)
					If $Index > UBound($aRndFuncList) - 1 Then $Index = UBound($aRndFuncList) - 1
					_RunFunction($aRndFuncList[$Index])
					_ArrayDelete($aRndFuncList, $Index)
				Else
					_RunFunction($aRndFuncList[0])
					ExitLoop
				EndIf
			WEnd
			If $RunState = False Then Return
			If $Restart = True Then ExitLoop
			If _Sleep($iDelayIdle1) Or $RunState = False Then ExitLoop
			$iCollectCounter = 0
		EndIf
		$iCollectCounter = $iCollectCounter + 1
		AddIdleTime()
		checkMainScreen(False) ; required here due to many possible exits
		If $CommandStop = -1 Then
			if 	$troops_maked_after_fullarmy = False and $actual_train_skip < $max_train_skip Then
				$troops_maked_after_fullarmy = False
				Train()
				If $Restart = True Then ExitLoop
				If _Sleep($iDelayIdle1) Then ExitLoop
				checkMainScreen(False)
			Else
				Setlog("Humanize bot, prevent to delete and recreate troops " & $actual_train_skip +1 & "/" & $max_train_skip,$color_blue)
				$actual_train_skip = $actual_train_skip +1
				if $actual_train_skip >= $max_train_skip Then
					$actual_train_skip = 0
					$troops_maked_after_fullarmy = false
				EndIf
				CheckOverviewFullArmy(True, False)  ; use true parameter to open train overview window
				getArmyHeroCount(False, False)
				getArmySpellCount(False, True) ; use true parameter to close train overview window
			EndIf
		EndIf
		If _Sleep($iDelayIdle1) Then Return
		If $CommandStop = 0 And $bTrainEnabled = True Then
			If Not ($fullArmy) Then
				if 	$troops_maked_after_fullarmy = False and $actual_train_skip <  $max_train_skip Then
					$troops_maked_after_fullarmy = False
					Train()
					If $Restart = True Then ExitLoop
					If _Sleep($iDelayIdle1) Then ExitLoop
					checkMainScreen(False)
				Else
					$actual_train_skip = $actual_train_skip +1
					if $actual_train_skip >= $max_train_skip Then
						$actual_train_skip = 0
						$troops_maked_after_fullarmy = false
					EndIf
					CheckOverviewFullArmy(True, False)  ; use true parameter to open train overview window
					getArmyHeroCount(False, False)
					getArmySpellCount(False, True) ; use true parameter to close train overview window
				EndIf
			EndIf
			If $fullArmy Then
				SetLog("Army Camp and Barracks are full, stop Training...", $COLOR_ACTION)
				$CommandStop = 3
			EndIf
		EndIf
		If _Sleep($iDelayIdle1) Then Return
		If $CommandStop = -1 Then
			DropTrophy()
				If $Restart = True Then ExitLoop
				If $fullArmy Then ExitLoop
				If _Sleep($iDelayIdle1) Then ExitLoop
				checkMainScreen(False)
		EndIf
		If _Sleep($iDelayIdle1) Then Return
		If $Restart = True Then ExitLoop
		$TimeIdle += Round(TimerDiff($hTimer) / 1000, 2) ;In Seconds

		If $canRequestCC = True Then RequestCC()

		If $CurCamp >=  $TotalCamp * $iEnableAfterArmyCamps[$DB]/100 and $iEnableSearchCamps[$DB]  = 1  and IsSearchModeActive($DB) Then Exitloop
		If $CurCamp >=  $TotalCamp * $iEnableAfterArmyCamps[$LB]/100 and $iEnableSearchCamps[$LB]  = 1  and IsSearchModeActive($LB) Then Exitloop
		If $CurCamp >=  $TotalCamp * $iEnableAfterArmyCamps[$TS]/100 and $iEnableSearchCamps[$TS]  = 1  and IsSearchModeActive($TS) Then Exitloop

		SetLog("Time Idle: " & StringFormat("%02i", Floor(Floor($TimeIdle / 60) / 60)) & ":" & StringFormat("%02i", Floor(Mod(Floor($TimeIdle / 60), 60))) & ":" & StringFormat("%02i", Floor(Mod($TimeIdle, 60))))

		If $OutOfGold = 1 Or $OutOfElixir = 1 Then Return  ; Halt mode due low resources, only 1 idle loop
		If ($CommandStop = 3 Or $CommandStop = 0) And $bTrainEnabled = False Then ExitLoop ; If training is not enabled, run only 1 idle loop

		If $iChkSnipeWhileTrain = 1 Then SnipeWhileTrain()  ;snipe while train

		If $CommandStop = -1 Then ; Check if closing bot/emulator while training and not in halt mode
			SmartWait4Train()
			If $Restart = True Then ExitLoop ; if smart wait activated, exit to runbot in case user adjusted GUI or left emulator/bot in bad state
		EndIf

	WEnd
EndFunc   ;==>Idle

Func AttackMain() ;Main control for attack functions
	;LoadAmountOfResourcesImages() ; for debug
	getArmyCapacity(true,true)
	If IsSearchAttackEnabled() Then
		If (IsSearchModeActive($DB) And checkCollectors(True, False)) or IsSearchModeActive($LB) or IsSearchModeActive($TS) Then
			If $iChkUseCCBalanced = 1 or $iChkUseCCBalancedCSV = 1 Then ;launch profilereport() only if option balance D/R it's activated
				ProfileReport()
				If _Sleep($iDelayAttackMain1) Then Return
				checkMainScreen(False)
				If $Restart = True Then Return
			EndIf
			If $iChkTrophyRange = 1 and Number($iTrophyCurrent) > Number($iTxtMaxTrophy) Then ;If current trophy above max trophy, try drop first
				DropTrophy()
				$Is_ClientSyncError = False ; reset OOS flag to prevent looping.
				If _Sleep($iDelayAttackMain1) Then Return
				Return ; return to runbot, refill armycamps
			EndIf
			If $debugsetlog = 1 Then
				SetLog(_PadStringCenter(" Hero status check" & BitAND($iHeroAttack[$DB], $iHeroWait[$DB], $iHeroAvailable) & "|" & $iHeroWait[$DB] & "|" & $iHeroAvailable, 54, "="), $COLOR_DEBUG)
				SetLog(_PadStringCenter(" Hero status check" & BitAND($iHeroAttack[$LB], $iHeroWait[$LB], $iHeroAvailable) & "|" & $iHeroWait[$LB] & "|" & $iHeroAvailable, 54, "="), $COLOR_DEBUG)
				;Setlog("BullyMode: " & $OptBullyMode & ", Bully Hero: " & BitAND($iHeroAttack[$iTHBullyAttackMode], $iHeroWait[$iTHBullyAttackMode], $iHeroAvailable) & "|" & $iHeroWait[$iTHBullyAttackMode] & "|" & $iHeroAvailable, $COLOR_DEBUG)
			EndIf
			PrepareSearch()
				If $OutOfGold = 1 Then Return ; Check flag for enough gold to search
				If $Restart = True Then Return
			VillageSearch()
				If $OutOfGold = 1 Then Return ; Check flag for enough gold to search
				If $Restart = True Then Return
			PrepareAttack($iMatchMode)
				If $Restart = True Then Return
			Attack()
				If $Restart = True Then Return
			ReturnHome($TakeLootSnapShot)
				If _Sleep($iDelayAttackMain2) Then Return
			Return True
		Else
			Setlog("No one of search condition match:", $COLOR_WARNING)
			Setlog("Waiting on troops, heroes and/or spells according to search settings", $COLOR_WARNING)
		EndIf
	Else
		SetLog("Attacking Not Planned, Skipped..", $COLOR_WARNING)
	EndIf
EndFunc   ;==>AttackMain

Func Attack() ;Selects which algorithm
	SetLog(" ====== Start Attack ====== ", $COLOR_SUCCESS)
	If  ($iMatchMode = $DB and $iAtkAlgorithm[$DB] = 1) or ($iMatchMode = $LB and  $iAtkAlgorithm[$LB] = 1) Then
		If $debugsetlog=1 Then Setlog("start scripted attack",$COLOR_ERROR)
		Algorithm_AttackCSV()
	Elseif $iMatchMode= $DB and  $iAtkAlgorithm[$DB] = 2 Then
		If $debugsetlog=1 Then Setlog("start milking attack",$COLOR_ERROR)
		Alogrithm_MilkingAttack()
	Else
		If $debugsetlog=1 Then Setlog("start standard attack",$COLOR_ERROR)
		algorithm_AllTroops()
	EndIf
EndFunc   ;==>Attack

Func QuickAttack()
   Local   $quicklymilking=0
   Local   $quicklythsnipe=0
	getArmyCapacity(true,true)
   If ( $iAtkAlgorithm[$DB] = 2  and IsSearchModeActive($DB) ) or (IsSearchModeActive($TS) ) Then
	  VillageReport()
   EndIf
   $iTrophyCurrent = getTrophyMainScreen($aTrophies[0], $aTrophies[1])
   If ($iChkTrophyRange = 1 and Number($iTrophyCurrent) > Number($iTxtMaxTrophy) )  then
	  If $debugsetlog=1 Then Setlog("No quickly re-attack, need to drop tropies",$COLOR_DEBUG )
	  return False ;need to drop tropies
   EndIf
   If $iAtkAlgorithm[$DB] = 2  and IsSearchModeActive($DB) Then
	  If Int($CurCamp) >=  $TotalCamp * $iEnableAfterArmyCamps[$DB]/100 and $iEnableSearchCamps[$DB]  = 1   Then
		 If $debugsetlog=1 Then Setlog("Milking: Quickly re-attack " &  Int($CurCamp) & " >= " & $TotalCamp & " * " & $iEnableAfterArmyCamps[$DB] & "/100 " & "= " &   $TotalCamp * $iEnableAfterArmyCamps[$DB]/100 ,$COLOR_DEBUG )
		 return true ;milking attack OK!
	  Else
		 If $debugsetlog=1 Then Setlog("Milking: No Quickly re-attack:  cur. "  & Int($CurCamp) & "  need " & $TotalCamp * $iEnableAfterArmyCamps[$DB]/100 & " firststart = " &  ($quicklyfirststart)  ,$COLOR_DEBUG)
		 return false ;milking attack no restart.. no enough army
	  EndIf
   EndIf
   If IsSearchModeActive($TS) Then
	  If Int($CurCamp) >=  $TotalCamp * $iEnableAfterArmyCamps[$TS]/100 and $iEnableSearchCamps[$TS]  = 1  Then
		 If $debugsetlog=1 Then Setlog("THSnipe: Quickly re-attack " &  Int($CurCamp) & " >= " & $TotalCamp & " * " & $iEnableAfterArmyCamps[$TS] & "/100 " & "= " &   $TotalCamp * $iEnableAfterArmyCamps[$TS]/100 ,$COLOR_DEBUG )
		 return True ;ts snipe attack OK!
	  Else
		 If $debugsetlog=1 Then Setlog("THSnipe: No Quickly re-attack:  cur. "  & Int($CurCamp) & "  need " & $TotalCamp * $iEnableAfterArmyCamps[$TS]/100 & " firststart = " &  ($quicklyfirststart)  ,$COLOR_DEBUG)
		 return False ;ts snipe no restart... no enough army
	  EndIF
   EndIf

EndFunc

Func _RunFunction($action)
	SetDebugLog("_RunFunction: " & $action & " BEGIN", $COLOR_DEBUG2)
	Switch $action
		Case "Collect"
			Collect()
			_Sleep($iDelayRunBot1)
		Case "CheckTombs"
			CheckTombs()
			_Sleep($iDelayRunBot3)
		Case "ReArm"
			ReArm()
			_Sleep($iDelayRunBot3)
		Case "ReplayShare"
			ReplayShare($iShareAttackNow)
			_Sleep($iDelayRunBot3)
		Case "ReportNotify"
			ReportNotify()
			_Sleep($iDelayRunBot3)
		 Case "DonateCC"
			If $iSkipDonateNearFulLTroopsEnable = 1 Then getArmyCapacity(true,true)
			DonateCC()
			If _Sleep($iDelayRunBot1) = False Then checkMainScreen(False)
		Case "DonateCC,Train"
			If $iSkipDonateNearFulLTroopsEnable = 1 Then getArmyCapacity(true,true)
			DonateCC()
			If _Sleep($iDelayRunBot1) = False Then checkMainScreen(False)
			if 	$troops_maked_after_fullarmy = False and $actual_train_skip < $max_train_skip Then
				$troops_maked_after_fullarmy = False
				Train()
				_Sleep($iDelayRunBot1)
			Else
				Setlog("Humanize bot, prevent to delete and recreate troops " & $actual_train_skip +1 & "/" & $max_train_skip,$color_blue)
				$actual_train_skip = $actual_train_skip +1
				if $actual_train_skip >= $max_train_skip Then
					$actual_train_skip = 0
					$troops_maked_after_fullarmy = false
				EndIf
				CheckOverviewFullArmy(True, False)  ; use true parameter to open train overview window
				getArmyHeroCount(False, False)
				getArmySpellCount(False, True) ; use true parameter to close train overview window
			EndIf
		Case "BoostBarracks"
			BoostBarracks()
		Case "BoostSpellFactory"
			BoostSpellFactory()
		Case "BoostDarkSpellFactory"
			BoostDarkSpellFactory()
		Case "BoostKing"
			BoostKing()
		Case "BoostQueen"
			BoostQueen()
		Case "BoostWarden"
			BoostWarden()
		Case "RequestCC"
			RequestCC()
			If _Sleep($iDelayRunBot1) = False Then checkMainScreen(False)
		Case "Laboratory"
			Laboratory()
			If _Sleep($iDelayRunBot3) = False Then checkMainScreen(False)
		Case "UpgradeHeroes"
			UpgradeHeroes()
			_Sleep($iDelayRunBot3)
		Case "UpgradeBuilding"
			UpgradeBuilding()
			_Sleep($iDelayRunBot3)
		Case ""
			SetDebugLog("Function call doesn't support empty string, please review array size", $COLOR_ERROR)
		Case Else
			SetLog("Unknown function call: " & $action, $COLOR_ERROR)
	EndSwitch
	SetDebugLog("_RunFunction: " & $action & " END", $COLOR_DEBUG2)
EndFunc   ;==>_RunFunction

;~ MultyAccount

Func ActiveAccount()
	  SetLog("* * * * * * * MULTY ACCOUNT * * * * * * *", $COLOR_RED)
	  SetLog("* * * Switch to next Active Account * * *", $COLOR_RED)
	  $canRequestCC = True
	  $bDonationEnabled = True
	  Sleep(1500)
	  RequestCC()
	  ClickP($aAway, 1, 0, "#0000") ;Click Away
	  Sleep(1500)
	  $iShouldRearm = True
	  $FirstStart = True
	  $RunState = True
	  $iSwCount = 0
	  If $sCurrProfile = "[01] Main" Then
		 If $ichkMultyAcc2 = 1 Then
			SwitchAccount("Second")
		 ElseIf $ichkMultyAcc3 = 1 Then
			SwitchAccount("Third")
		 ElseIf $ichkMultyAcc4 = 1 Then
			SwitchAccount("Fourth")
		 ElseIf $ichkMultyAcc5 = 1 Then
			SwitchAccount("Fifth")
		 ElseIf $ichkMultyAcc6 = 1 Then
			SwitchAccount("Sixth")
		 EndIF
	  ElseIf $sCurrProfile = "[02] Second" Then
		 If $ichkMultyAcc3 = 1 Then
			SwitchAccount("Third")
		 ElseIf $ichkMultyAcc4 = 1 Then
			SwitchAccount("Fourth")
		 ElseIf $ichkMultyAcc5 = 1 Then
			SwitchAccount("Fifth")
		 ElseIf $ichkMultyAcc6 = 1 Then
			SwitchAccount("Sixth")
		 ElseIf $ichkMultyAcc1 = 1 Then
			SwitchAccount("Main")
		 EndIf
	  ElseIf $sCurrProfile = "[03] Third" Then
		 If $ichkMultyAcc4 = 1 Then
			SwitchAccount("Fourth")
		 ElseIf $ichkMultyAcc5 = 1 Then
			SwitchAccount("Fifth")
		 ElseIf $ichkMultyAcc6 = 1 Then
			SwitchAccount("Sixth")
		 ElseIf $ichkMultyAcc1 = 1 Then
			SwitchAccount("Main")
		 ElseIf $ichkMultyAcc2 = 1 Then
			SwitchAccount("Second")
		 EndIf
	  ElseIf $sCurrProfile = "[04] Fourth" Then
		 If $ichkMultyAcc5 = 1 Then
			SwitchAccount("Fifth")
		 ElseIf $ichkMultyAcc6 = 1 Then
			SwitchAccount("Sixth")
		 ElseIf $ichkMultyAcc1 = 1 Then
			SwitchAccount("Main")
		 ElseIf $ichkMultyAcc2 = 1 Then
			SwitchAccount("Second")
		 ElseIf $ichkMultyAcc3 = 1 Then
			SwitchAccount("Third")
		 EndIf
	  ElseIf $sCurrProfile = "[05] Fifth" Then
		 If $ichkMultyAcc6 = 1 Then
			SwitchAccount("Sixth")
		 ElseIf $ichkMultyAcc1 = 1 Then
			SwitchAccount("Main")
		 ElseIf $ichkMultyAcc2 = 1 Then
			SwitchAccount("Second")
		 ElseIf $ichkMultyAcc3 = 1 Then
			SwitchAccount("Third")
		 ElseIf $ichkMultyAcc4 = 1 Then
			SwitchAccount("Fourth")
		 EndIf
	  ElseIf $sCurrProfile = "[06] Sixth" Then
		 If $ichkMultyAcc1 = 1 Then
			SwitchAccount("Main")
		 ElseIf $ichkMultyAcc2 = 1 Then
			SwitchAccount("Second")
		 ElseIf $ichkMultyAcc3 = 1 Then
			SwitchAccount("Third")
		 ElseIf $ichkMultyAcc4 = 1 Then
			SwitchAccount("Fourth")
		 ElseIf $ichkMultyAcc5 = 1 Then
			SwitchAccount("Fifth")
		 EndIf
	  EndIf
   EndFunc
Func DetectAccount()
	While 1
	  ZoomOut()
	ExitLoop
	WEnd
	Sleep (2000)
	_CaptureRegion()
Local $hBMP_Cropped = _GDIPlus_BitmapCloneArea($hBitmap, 50, 0,  140, 18)
Local $hHBMP_Cropped = _GDIPlus_BitmapCreateHBITMAPFromBitmap($hBMP_Cropped)

If  Not FileExists(@ScriptDir & "\images\MultyAccount\main.bmp") And $ichkMultyAcc1 = 1 Then
	 _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\main.bmp")
ElseIf  Not FileExists(@ScriptDir & "\images\MultyAccount\Second.bmp") And $ichkMultyAcc2 = 1 Then
	_GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\Second.bmp")
ElseIf  Not FileExists(@ScriptDir & "\images\MultyAccount\Third.bmp") And $ichkMultyAcc3 = 1 Then
	 _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\Third.bmp")
ElseIf  Not FileExists(@ScriptDir & "\images\MultyAccount\Fourth.bmp") And $ichkMultyAcc4 = 1 Then
	 _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\Fourth.bmp")
ElseIf  Not FileExists(@ScriptDir & "\images\MultyAccount\Fifth.bmp") And $ichkMultyAcc5 = 1 Then
	 _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\Fifth.bmp")
ElseIf  Not FileExists(@ScriptDir & "\images\MultyAccount\Sixth.bmp") And $ichkMultyAcc6 = 1 Then
	 _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\Sixth.bmp")
EndIf

If FileExists(@ScriptDir & "\images\MultyAccount\temp.bmp") Then
   FileDelete(@ScriptDir & "\images\MultyAccount\temp.bmp")
EndIf

	_GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\temp.bmp")
	_GDIPlus_ImageDispose($hBitmap)

$bm1 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\main.bmp")
$bm2 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\temp.bmp")
$bm3 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Second.bmp")
$bm4 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Third.bmp")
$bm5 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Fourth.bmp")
$bm6 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Fifth.bmp")
$bm7 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Sixth.bmp")

	If $ichkMultyAcc1 = 1 And CompareBitmaps($bm1, $bm2) = "True" Then
		If $sCurrProfile = "[01] Main" Then
		    SetLog("Main account already loaded...", $COLOR_BLUE)
		Else
			SetLog("Main account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 0)
			cmbProfile()
		EndIf
	ElseIf $ichkMultyAcc2 = 1 And  CompareBitmaps($bm3, $bm2) = "True" Then
		If $sCurrProfile = "[02] Second" Then
			SetLog("Second account already loaded...", $COLOR_BLUE)
		Else
			SetLog("Second account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 1)
			cmbProfile()
		EndIf
	ElseIf $ichkMultyAcc3 = 1 And  CompareBitmaps($bm4, $bm2) = "True" Then
		If $sCurrProfile = "[03] Third" Then
			SetLog("Third account already loaded...", $COLOR_BLUE)
		Else
			SetLog("Third account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 2)
			cmbProfile()
		EndIf
	ElseIf $ichkMultyAcc4 = 1 And  CompareBitmaps($bm5, $bm2) = "True" Then
		If $sCurrProfile = "[04] Fourth" Then
			SetLog("Fourth account already loaded...", $COLOR_BLUE)
		Else
			SetLog("Fourth account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 3)
			cmbProfile()
		EndIf
	ElseIf $ichkMultyAcc5 = 1 And  CompareBitmaps($bm6, $bm2) = "True" Then
		If $sCurrProfile = "[05] Fifth" Then
			SetLog("Fifth account already loaded...", $Color_Black)
		Else
			SetLog("Fifth account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 4)
			cmbProfile()
		EndIf

	ElseIf $ichkMultyAcc6 = 1 And  CompareBitmaps($bm7, $bm2) = "True" Then
		If $sCurrProfile = "[06] Sixth" Then
			SetLog("Sixth account already loaded...", $COLOR_BLUE)
		Else
			SetLog("Sixth account Detected...", $COLOR_GREEN)
			_GUICtrlComboBox_SetCurSel($cmbProfile, 5)
			cmbProfile()
		 EndIf
    Else
		 SetLog(_PadStringCenter(" IMAGE COMPARISON ERROR ", 50, "*"), $COLOR_ORANGE)
		 SetLog(_PadStringCenter(" HOPE GET FIXED NEXT RUN ;) ", 50, "*"), $COLOR_ORANGE)
		 _GDIPlus_ImageDispose($bm1)
		 _GDIPlus_ImageDispose($bm2)
		 _GDIPlus_ImageDispose($bm3)
		 _GDIPlus_ImageDispose($bm4)
		 _GDIPlus_ImageDispose($bm5)
		 _GDIPlus_ImageDispose($bm6)
		 _GDIPlus_ImageDispose($bm7)
	  EndIf
_GDIPlus_ImageDispose($bm1)
_GDIPlus_ImageDispose($bm2)
_GDIPlus_ImageDispose($bm3)
_GDIPlus_ImageDispose($bm4)
_GDIPlus_ImageDispose($bm5)
_GDIPlus_ImageDispose($bm6)
_GDIPlus_ImageDispose($bm7)

EndFunc

Func CompareBitmaps($bm1, $bm2)
    $Bm1W = _GDIPlus_ImageGetWidth($bm1)
    $Bm1H = _GDIPlus_ImageGetHeight($bm1)
    $BitmapData1 = _GDIPlus_BitmapLockBits($bm1, 0, 0, $Bm1W, $Bm1H, $GDIP_ILMREAD, $GDIP_PXF32RGB)
    $Stride = DllStructGetData($BitmapData1, "Stride")
    $Scan0 = DllStructGetData($BitmapData1, "Scan0")
    $ptr1 = $Scan0
    $size1 = ($Bm1H - 1) * $Stride + ($Bm1W - 1) * 4
    $Bm2W = _GDIPlus_ImageGetWidth($bm2)
    $Bm2H = _GDIPlus_ImageGetHeight($bm2)
    $BitmapData2 = _GDIPlus_BitmapLockBits($bm2, 0, 0, $Bm2W, $Bm2H, $GDIP_ILMREAD, $GDIP_PXF32RGB)
    $Stride = DllStructGetData($BitmapData2, "Stride")
    $Scan0 = DllStructGetData($BitmapData2, "Scan0")
    $ptr2 = $Scan0
    $size2 = ($Bm2H - 1) * $Stride + ($Bm2W - 1) * 4
    $smallest = $size1
    If $size2 < $smallest Then $smallest = $size2
    $call = DllCall("msvcrt.dll", "int:cdecl", "memcmp", "ptr", $ptr1, "ptr", $ptr2, "int", $smallest)
    _GDIPlus_BitmapUnlockBits($bm1, $BitmapData1)
    _GDIPlus_BitmapUnlockBits($bm2, $BitmapData2)
    Return ($call[0]=0)
 EndFunc

Func DetectCurrentAccount($CheckAccountID)

	While 1
		ZoomOut()
	ExitLoop
	WEnd
	Sleep (2000)
	_CaptureRegion()
	Local $hBMP_Cropped = _GDIPlus_BitmapCloneArea($hBitmap, 50, 0,  140, 18)
	Local $hHBMP_Cropped = _GDIPlus_BitmapCreateHBITMAPFromBitmap($hBMP_Cropped)

	if Not FileExists(@ScriptDir & "\images\MultyAccount\" & $CheckAccountID & ".bmp") Then
		SetLog("MultyAccount File \images\MultyAccount\main.bmp is missing.", $COLOR_BLUE)
		Return False
	EndIf
	if FileExists(@ScriptDir & "\images\MultyAccount\temp.bmp") Then
	   FileDelete(@ScriptDir & "\images\MultyAccount\temp.bmp")
	EndIf

	  _GDIPlus_ImageSaveToFile($hBMP_Cropped, @ScriptDir & "\images\MultyAccount\temp.bmp")
	  _GDIPlus_ImageDispose($hBitmap)

	$bm1 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\main.bmp")
	$bm3 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Second.bmp")
	$bm2 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\temp.bmp")
	$bm4 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Third.bmp")
	$bm5 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Fourth.bmp")
	$bm6 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Fifth.bmp")
	$bm7 = _GDIPlus_ImageLoadFromFile (@ScriptDir & "\images\MultyAccount\Sixth.bmp")

	If $CheckAccountID = "Main" AND CompareBitmaps($bm1, $bm2) Then
		SetLog("Main account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	ElseIf $CheckAccountID = "Second" AND CompareBitmaps($bm3, $bm2) Then
		SetLog("Second account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	ElseIf $CheckAccountID = "Third" AND CompareBitmaps($bm4, $bm2) Then
		SetLog("Third account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	ElseIf $CheckAccountID = "Fourth" AND CompareBitmaps($bm5, $bm2) Then
		SetLog("Fourth account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	ElseIf $CheckAccountID = "Fifth" AND CompareBitmaps($bm6, $bm2) Then
		SetLog("Fifth account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	ElseIf $CheckAccountID = "Sixth" AND CompareBitmaps($bm7, $bm2) Then
		SetLog("Sixth account Detected. No switch is required.", $COLOR_RED)
		_GDIPlus_ImageDispose($bm1)
		_GDIPlus_ImageDispose($bm2)
		_GDIPlus_ImageDispose($bm3)
		_GDIPlus_ImageDispose($bm4)
		_GDIPlus_ImageDispose($bm5)
		_GDIPlus_ImageDispose($bm6)
		_GDIPlus_ImageDispose($bm7)
		Return False
	EndIf

	_GDIPlus_ImageDispose($bm1)
	_GDIPlus_ImageDispose($bm2)
	_GDIPlus_ImageDispose($bm3)
	_GDIPlus_ImageDispose($bm4)
	_GDIPlus_ImageDispose($bm5)
	_GDIPlus_ImageDispose($bm6)
	_GDIPlus_ImageDispose($bm7)

	Return True
 EndFunc  ;==>DetectCurrentAccount

Func MultiFarming()
	If GUICtrlRead($chkMultyAccount) = $GUI_CHECKED Then
		$ichkMultyAccount = 1
		If GUICtrlRead($chkMultyAcc1) = $GUI_CHECKED Then
			$ichkMultyAcc1 = 1
		Else
			$ichkMultyAcc1 = 0
		EndIf
		If GUICtrlRead($chkMultyAcc2) = $GUI_CHECKED Then
			$ichkMultyAcc2 = 1
		Else
			$ichkMultyAcc2 = 0
		EndIf
		If GUICtrlRead($chkMultyAcc3) = $GUI_CHECKED Then
			$ichkMultyAcc3 = 1
		Else
			$ichkMultyAcc3 = 0
		EndIf
		If GUICtrlRead($chkMultyAcc4) = $GUI_CHECKED Then
			$ichkMultyAcc4 = 1
		Else
			$ichkMultyAcc4 = 0
		EndIf
		If GUICtrlRead($chkMultyAcc5) = $GUI_CHECKED Then
			$ichkMultyAcc5 = 1
		Else
			$ichkMultyAcc5 = 0
		EndIf
		If GUICtrlRead($chkMultyAcc6) = $GUI_CHECKED Then
			$ichkMultyAcc6 = 1
		Else
			$ichkMultyAcc6 = 0
		EndIf
		GUICtrlSetState($lblmultyAcc, $GUI_ENABLE)
		For $i = $grpControls To $cmbHoursStop
			GUICtrlSetState($i, $GUI_DISABLE)
		Next
		If GUICtrlRead($chkBotStop) = $GUI_CHECKED Then
			GUICtrlSetState($chkBotStop, $GUI_UNCHECKED)
		EndIf
	Else
		$ichkMultyAccount = 0
		GUICtrlSetState($lblmultyAcc, $GUI_DISABLE)
		For $i = $grpControls To $cmbHoursStop
			GUICtrlSetState($i, $GUI_ENABLE)
		Next
	EndIf

	GUICtrlSetState($btnmultyAcc1, $GUI_DISABLE)
	GUICtrlSetState($btnmultyAcc2, $GUI_DISABLE)
	GUICtrlSetState($btnmultyAcc3, $GUI_DISABLE)
	GUICtrlSetState($btnmultyAcc4, $GUI_DISABLE)
	GUICtrlSetState($btnmultyAcc5, $GUI_DISABLE)
	GUICtrlSetState($btnmultyAcc6, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc1, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc2, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc3, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc4, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc5, $GUI_DISABLE)
	GUICtrlSetState($chkMultyAcc6, $GUI_DISABLE)

	If  FileExists(@ScriptDir & "\images\MultyAccount\Accmain.bmp") Then
		GUICtrlSetState($btnmultyAcc1, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc1, $GUI_ENABLE)
	EndIf
	If  FileExists(@ScriptDir & "\images\MultyAccount\AccSecond.bmp") Then
		GUICtrlSetState($btnmultyAcc2, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc2, $GUI_ENABLE)
	EndIf
	If  FileExists(@ScriptDir & "\images\MultyAccount\AccThird.bmp") Then
		GUICtrlSetState($btnmultyAcc3, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc3, $GUI_ENABLE)
	EndIf
	If  FileExists(@ScriptDir & "\images\MultyAccount\AccFourth.bmp") Then
		GUICtrlSetState($btnmultyAcc4, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc4, $GUI_ENABLE)
	EndIf
	If  FileExists(@ScriptDir & "\images\MultyAccount\AccFifth.bmp") Then
		GUICtrlSetState($btnmultyAcc5, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc5, $GUI_ENABLE)
	EndIf
	If  FileExists(@ScriptDir & "\images\MultyAccount\AccSixth.bmp") Then
		GUICtrlSetState($btnmultyAcc6, $GUI_ENABLE)
		GUICtrlSetState($chkMultyAcc6, $GUI_ENABLE)
	EndIf

EndFunc   ;==>MultiFarming

Func btnmultyDetectAcc()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("MultyAccount Detection Requested ...", $COLOR_BLUE)
	$RunState = True
	waitMainScreen()
	If IsMainPage()  Then
		DetectAccount()
	Else
		SetLog("MultyAccount Detection Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Main Account
Func btnmultyAcc1()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Main Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	If IsMainPage() AND DetectCurrentAccount("Main") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Main")
		checkMainScreen()
		DetectAccount()
		SetLog("Main Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Second Account
Func btnmultyAcc2()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Second Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	SetLog("Second Account Switch In Progress ...", $COLOR_BLUE)
	If IsMainPage() AND DetectCurrentAccount("Second") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Second")
		checkMainScreen()
		DetectAccount()
		SetLog("Second Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Third Account
Func btnmultyAcc3()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Third Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	SetLog("Third Account Switch In Progress ...", $COLOR_BLUE)
	If IsMainPage() AND DetectCurrentAccount("Third") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Third")
		checkMainScreen()
		DetectAccount()
		SetLog("Third Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Fourth Account
Func btnmultyAcc4()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Fourth Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	SetLog("Fourth Account Switch In Progress ...", $COLOR_BLUE)
	If IsMainPage() AND DetectCurrentAccount("Fourth") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Fourth")
		checkMainScreen()
		DetectAccount()
		SetLog("Fourth Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Fifth Account
Func btnmultyAcc5()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Fifth Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	SetLog("Fifth Account Switch In Progress ...", $COLOR_BLUE)
	If IsMainPage() AND DetectCurrentAccount("Fifth") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Fifth")
		checkMainScreen()
		DetectAccount()
		SetLog("Fifth Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc
;Sixth Account
Func btnmultyAcc6()
	If $RunState Then Return
	LockGUI()
	SetLog("==================== Bot Start ====================", $COLOR_GREEN)
	Sleep(2000)
	SetLog("Sixth Account Switch Requested ...", $COLOR_BLUE)
		$RunState = True
	waitMainScreen()
	SetLog("Sixth Account Switch In Progress ...", $COLOR_BLUE)
	If IsMainPage() AND DetectCurrentAccount("Sixth") Then
		checkMainScreen()
		$iSwCount = 0
		SwitchAccount("Sixth")
		checkMainScreen()
		DetectAccount()
		SetLog("Sixth Account Switch Completed", $COLOR_BLUE)
	Else
		SetLog("MultyAccount Switch Canceled", $COLOR_RED)
	EndIf
	$RunState = False
	UnLockGUI()
EndFunc ;==> Smart Switch

Func LockGUI()
		GUICtrlSetState($btnStart, $GUI_HIDE)
		GUICtrlSetState($btnStop, $GUI_SHOW)
		GUICtrlSetState($btnPause, $GUI_SHOW)
		GUICtrlSetState($btnResume, $GUI_HIDE)
		GUICtrlSetState($btnSearchMode, $GUI_HIDE)

		$bTrainEnabled = True
		$bDonationEnabled = True
		$MeetCondStop = False
		$Is_ClientSyncError = False
		$bDisableBreakCheck = False ; reset flag to check for early warning message when bot start/restart in case user stopped in middle
		$bDisableDropTrophy = False ; Reset Disabled Drop Trophy because the user has no Tier 1 or 2 Troops

		If Not $bSearchMode Then
			CreateLogFile()
			CreateAttackLogFile()
			If $FirstRun = -1 Then $FirstRun = 1
		EndIf

		_GUICtrlEdit_SetText($txtLog, _PadStringCenter(" BOT LOG ", 71, "="))
		_GUICtrlRichEdit_SetFont($txtLog, 6, "Lucida Console")
		_GUICtrlRichEdit_AppendTextColor($txtLog, "" & @CRLF, _ColorConvert($Color_Black))
#cs
 		$GUIControl_Disabled = True
		For $i = $FirstControlToHide To $LastControlToHide ; Save state of all controls on tabs
			If IsTab($i) Or IsAlwaysEnabledControl($i) Then ContinueLoop
			If $PushBulletEnabled And $i = $btnDeletePBmessages Then ContinueLoop ; Modified by CDudz
			If $i = $btnMakeScreenshot Then ContinueLoop ; exclude
			If $i = $divider Then ContinueLoop ; exclude divider
				; Save state of all controls on tabs
				$iPrevState[$i] = GUICtrlGetState($i)
				GUICtrlSetState($i, $GUI_DISABLE)
		Next
		For $i = $FirstControlToHideMOD To $LastControlToHideMOD ; Save state of all controls on tabs
			If IsTab($i) Or IsAlwaysEnabledControl($i) Then ContinueLoop
			If $PushBulletEnabled And $i = $btnDeletePBmessages Then ContinueLoop ; exclude the DeleteAllMesages button when PushBullet is enabled
			If $i = $btnMakeScreenshot Then ContinueLoop ; exclude
			If $i = $divider Then ContinueLoop ; exclude divider
				; Save state of all controls on tabs
				$iPrevState[$i] = GUICtrlGetState($i)
				GUICtrlSetState($i, $GUI_DISABLE)
Next
#ce
		$GUIControl_Disabled = False
		SetRedrawBotWindow(True)
	 EndFunc   ;==>LockGUI

Func UnLockGUI()
		GUICtrlSetState($chkBackground, $GUI_ENABLE)
		GUICtrlSetState($btnStart, $GUI_SHOW)
		GUICtrlSetState($btnStop, $GUI_HIDE)
		GUICtrlSetState($btnPause, $GUI_HIDE)
		GUICtrlSetState($btnResume, $GUI_HIDE)
		If $iTownHallLevel > 2 Then GUICtrlSetState($btnSearchMode, $GUI_ENABLE)
		GUICtrlSetState($btnSearchMode, $GUI_SHOW)

		; hide attack buttons if show
		GUICtrlSetState($btnAttackNowDB, $GUI_HIDE)
		GUICtrlSetState($btnAttackNowLB, $GUI_HIDE)
		GUICtrlSetState($btnAttackNowTS, $GUI_HIDE)
		GUICtrlSetState($pic2arrow, $GUI_SHOW)
		GUICtrlSetState($lblVersion, $GUI_SHOW)

		SetDebugLog("Enable GUI Controls")
		SetRedrawBotWindow(False)
#cs
		$GUIControl_Disabled = True
		For $i = $FirstControlToHide To $LastControlToHide ; Save state of all controls on tabs
			If IsTab($i) Or IsAlwaysEnabledControl($i) Then ContinueLoop
			If $PushBulletEnabled And $i = $btnDeletePBmessages Then ContinueLoop ; Modified by CDudz
			If $i = $btnMakeScreenshot Then ContinueLoop ; exclude
			If $i = $divider Then ContinueLoop ; exclude divider
			; Restore previous state of controls
			GUICtrlSetState($i, $iPrevState[$i])
		Next
		For $i = $FirstControlToHideMOD To $LastControlToHideMOD ; Restore previous state of controls
			If IsTab($i) Or IsAlwaysEnabledControl($i) Then ContinueLoop
			If $PushBulletEnabled And $i = $btnDeletePBmessages Then ContinueLoop ; exclude the DeleteAllMesages button when PushBullet is enabled
			If $i = $btnMakeScreenshot Then ContinueLoop ; exclude
			If $i = $divider Then ContinueLoop ; exclude divider
			; Restore previous state of controls
			GUICtrlSetState($i, $iPrevState[$i])
Next
#ce
		$GUIControl_Disabled = False

		AndroidBotStopEvent() ; signal android that bot is now stopping

		SetLog(_PadStringCenter(" Bot Stop ", 50, "="), $COLOR_ORANGE)
		SetRedrawBotWindow(True) ; must be here at bottom, after SetLog, so Log refreshes. You could also use SetRedrawBotWindow(True, False) and let the events handle the refresh.

EndFunc   ;==>UnLockGUI

Func SwitchAccount($bAccount)
	$iConfirm = 0
	$AccImg = @ScriptDir & "\images\MultyAccount\Acc" & $bAccount & ".bmp"
	If Not FileExists($AccImg) Then
		SetLog("Acc" & $bAccount & ".bmp Not Found ", $COLOR_RED)
		Return False
	EndIf
	checkMainScreen()
	Send("{CapsLock off}")
	PureClick(830, 590, 1, 0, "Click Settings") ;Click Switch
	If _Sleep(2000) Then Return ;1000

	SelectAccount($bAccount)
	If $RunState = False Then Return

	If $iConfirm = 1 Then
		FileDelete((@ScriptDir & "\images\MultyAccount\" & $bAccount & ".bmp"))
	EndIf
	$fullArmy = False
	Local $iLoopCount = 0
	While 1
		Local $Message = _PixelSearch(487, 387, 492, 391, Hex(0xE8E8E0, 6), 0) ;load pixel
		If IsArray($Message) Then
			SetLog("Loading Account In Progress...", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			SetLog("Loading Load Button: 1", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 2", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 3", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			PureClick(512, 433, 1, 0, "Click Load") ;Click Load Button
			If _Sleep(1000) Then Return

			Local $Message = _PixelSearch(470, 249 + $midOffsetY, 478, 255 + $midOffsetY, Hex(0xE8E8E0, 6), 0)
			If IsArray($Message) Then
				$iConfirm = 1
				PureClick(521, 198) ;Click Confirm
				If _Sleep(1500) Then Return
				PureClick(339, 215, 1, 0, "Click Textbox") ;Click Confirm textbox
				SetLog("Insert CONFIRM To Text Box ", $COLOR_BLUE)
				If _Sleep(2000) Then Return
				If SendText("CONFIRM") = 0 Then ;Insert CONFIRM To Text
					Setlog("Error Insert CONFIRM To Text Box", $COLOR_RED)
					Return
				EndIf
				If _Sleep(2000) Then Return
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Success", $COLOR_GREEN)
				If _SleepStatus(7000) Then Return
			Else
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Fail", $COLOR_ORANGE)
				If _SleepStatus(5000) Then Return
			EndIf
			ExitLoop
		EndIf

		$iLoopCount += 1
		ConsoleWrite($iLoopCount & @CRLF)
		If $iLoopCount > 1000 Then
			ExitLoop
		EndIf
	WEnd
EndFunc

Func SelectAccount($bAccount)
	Local $iLoopCount = 0
	Local Const $XCon = 431
	Local Const $YCon = 434
	If _ColorCheck(_GetPixelColor($XCon, $YCon, True), Hex(4284458031, 6), 20) Then
		PureClick($XCon, $YCon, 1, 0, "Click Connected") ;Click Connect
		If _Sleep(2500) Then Return
	Else
		PureClick($XCon, $YCon, 1, 0, "Click Disconnected") ;Click Disconn
		If _Sleep(800) Then Return
	EndIf
		PureClick($XCon, $YCon, 1, 0, "Click Disconnected") ;Click Disconn
	$iSwCount = 0
	If $iSwCount > 6 Then
		SetLog(" Exit Now ...Cancel change account")
		SetLog("PLease make sure image create From png", $COLOR_RED)
		PureClickP($aAway, 2, 250, "#0291")
		Return
	ElseIf IsMainPage() Then
		Setlog("Change account cancel")
		Return True
	EndIf
		If _Sleep(5000) Then Return
	While 1
		Local $Message = _PixelSearch(230, 235 + $midOffsetY, 232, 455 + $midOffsetY, Hex(0xF5F5F5, 6), 0) ;(164, 45 + $midOffsetY, 166, 281 + $midOffsetY, Hex(0x689F38, 6), 0)
		Local $Message1 = _PixelSearch(230, 235 + $midOffsetY, 232, 455 + $midOffsetY, Hex(0xF5F5F5, 6), 0) ;(164, 45 + $midOffsetY, 166, 281 + $midOffsetY, Hex(0xF5F5F5, 6), 0)
		If IsArray($Message) Then
			SetLog("Searching " & $bAccount & " Account...", $COLOR_BLUE)
			If _Sleep(1500) Then Return
			CheckAccount($bAccount)
			FileDelete(@ScriptDir & "\images\Multyfarming\$bAccount.bmp")
			ExitLoop
		ElseIf IsArray($Message1) Then
			SetLog("Searching " & $bAccount & " Account...", $COLOR_ORANGE)
			If _Sleep(1500) Then Return
			CheckAccount($bAccount)
			ExitLoop
		EndIf
		If _Sleep(1200) Then Return
		$iLoopCount += 1
		ConsoleWrite($iLoopCount & @CRLF)
		If $iLoopCount > 60 Then
			PureClick(778, 115, 1, 0, "Click Close") ;Click Close
			SetLog("No Account Detected, Sorry..", $COLOR_PURPLE)
			If _Sleep(2000) Then Return
			PureClick(778, 115, 1, 0, "Click Close") ;Click Close
			CloseCoC()
			If _Sleep(1500) Then Return
			SwitchAccount($bAccount)
			ExitLoop
		EndIf
	WEnd
	If $AccountLoc = 1 Then
		LoadAccount ($bAccount)
	EndIf

EndFunc

Func LoadAccount($bAccount)
	Local $iLoopCount = 0
	While 1
		Local $Message = _PixelSearch(487, 387, 492, 391, Hex(0xE8E8E0, 6), 0) ;load pixel
		If IsArray($Message) Then
			SetLog("Loading Account In Progress...", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			SetLog("Loading Load Button: 1", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 2", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 3", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			PureClick(512, 433, 1, 0, "Click Load") ;Click Load Button
			If _Sleep(1000) Then Return

			Local $Message = _PixelSearch(470, 249 + $midOffsetY, 478, 255 + $midOffsetY, Hex(0xE8E8E0, 6), 0)
			If IsArray($Message) Then
				$iConfirm = 1
				PureClick(521, 198) ;Click Confirm
				If _Sleep(1500) Then Return
				PureClick(339, 215, 1, 0, "Click Textbox") ;Click Confirm textbox
				SetLog("Insert CONFIRM To Text Box ", $COLOR_BLUE)
				If _Sleep(2000) Then Return
				If SendText("CONFIRM") = 0 Then ;Insert CONFIRM To Text
					Setlog("Error Insert CONFIRM To Text Box", $COLOR_RED)
					Return
				EndIf
				If _Sleep(2000) Then Return
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Success", $COLOR_GREEN)
				If _SleepStatus(7000) Then Return
			Else
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Fail", $COLOR_ORANGE)
				If _SleepStatus(5000) Then Return
			EndIf
			ExitLoop
		EndIf

		$iLoopCount += 1
		ConsoleWrite($iLoopCount & @CRLF)
		If $iLoopCount > 5000 Then
			SetLog("Slow Open Window Msg Load, Please Wait..", $COLOR_PURPLE)
			LoadAccount2($bAccount)
			ExitLoop
		EndIf
	WEnd
 EndFunc

Func LoadAccount2($bAccount)
	Local $iLoopCount = 0
	While 1
		Local $Message = _PixelSearch(487, 387, 492, 391, Hex(0xE8E8E0, 6), 0) ;load pixel
		If IsArray($Message) Then
			SetLog("Loading Account In Progress...", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			SetLog("Loading Load Button: 1", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 2", $COLOR_BLUE)
			If _Sleep(1000) Then Return ;Not
			SetLog("Loading Load Button: 3", $COLOR_BLUE)
			If _Sleep(500) Then Return ;Not
			PureClick(512, 433, 1, 0, "Click Load") ;Click Load Button
			If _Sleep(1000) Then Return

			Local $Message = _PixelSearch(470, 249 + $midOffsetY, 478, 255 + $midOffsetY, Hex(0xE8E8E0, 6), 0)
			If IsArray($Message) Then
				$iConfirm = 1
				PureClick(521, 198) ;Click Confirm
				If _Sleep(1500) Then Return
				PureClick(339, 215, 1, 0, "Click Textbox") ;Click Confirm textbox
				SetLog("Insert CONFIRM To Text Box ", $COLOR_BLUE)
				If _Sleep(2000) Then Return
				If SendText("CONFIRM") = 0 Then ;Insert CONFIRM To Text
					Setlog("Error Insert CONFIRM To Text Box", $COLOR_RED)
					Return
				EndIf
				If _Sleep(2000) Then Return
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Success", $COLOR_GREEN)
				If _SleepStatus(7000) Then Return
			Else
				PureClick(521, 200, 1, 0, "Click Okay") ;Click Confirm
				SetLog("Click Okay Fail..", $COLOR_ORANGE)
				If _SleepStatus(5000) Then Return
			EndIf
			ExitLoop
		EndIf

		$iLoopCount += 1
		ConsoleWrite($iLoopCount & @CRLF)
		If $iLoopCount > 4000 Then
			SetLog("Not Open Window Msg Load, Please Wait..", $COLOR_PURPLE)
			;CloseCoC()
			RestartAndroidCoC()
			SwitchAccount($bAccount)
			ExitLoop
		EndIf
	WEnd
EndFunc ; LoadAccount2

Func CheckOK()

	Local $OkX, $OkY
	$Ok = @ScriptDir & "\images\MultyAccount\Ok.bmp"
	If Not FileExists($Ok) Then Return False
	$OkLoc = 0
	_CaptureRegion()
	If _Sleep(500) Then Return
	For $OkTol = 0 To 20
		If $OkLoc = 0 Then
			$OkX = 0
			$OkY = 0
			$OkLoc = _ImageSearch($Ok, 1, $OkX, $OkY, $OkTol)
			If $OkLoc = 1 Then
				SetLog("Found Ok Button ", $COLOR_GREEN)
				If $DebugSetLog = 1 Then SetLog("Ok Button found (" & $OkX & "," & $OkY & ") tolerance:" & $OkTol, $COLOR_PURPLE)
				PureClick($OkX, $OkY,1,0,"#0120")
				If _Sleep(500) Then Return
				Return True
			EndIf
		EndIf
	Next
	If $DebugSetLog = 1 Then SetLog("Cannot find OK Button", $COLOR_PURPLE)
	If _Sleep(500) Then Return
	EndFunc   ;==>CheckOK Button

Func CheckAccount($bAccount)

	Local $AccountX, $AccountY
	$AccImg = @ScriptDir & "\images\MultyAccount\Acc" & $bAccount & ".bmp"
	If Not FileExists($AccImg) Then
		SetLog("Acc" & $bAccount & ".bmp Not Found ", $COLOR_RED)
		Return False
	EndIf
	$AccountLoc = 0
	_CaptureRegion()
	If _Sleep(500) Then Return
	For $AccountTol = 0 To 20
		If $AccountLoc = 0 Then
			$AccountX = 0
			$AccountY = 0
			$AccountLoc = _ImageSearch($AccImg, 1, $AccountX, $AccountY, $AccountTol)
			If $AccountLoc = 1 Then
				SetLog("Found " & $bAccount & " Account..", $COLOR_GREEN)
				If $DebugSetLog = 1 Then SetLog("Found " & $bAccount & " Account (" & $AccountX & "," & $AccountY & ") tolerance:" & $AccountTol, $COLOR_PURPLE)
				PureClick($AccountX, $AccountY, 1, 0, "Click Account")
				If _Sleep(500) Then Return
				Return True
			EndIf
		EndIf
	Next
	If $DebugSetLog = 1 Then SetLog("Cannot Found " & $bAccount & " Account", $COLOR_PURPLE)
	If _Sleep(500) Then Return
EndFunc ; Switch Account

Func ProfileSwitch()
   Local $SwitchtoProfile = ""
   If $SwitchtoProfile <> "" Then
	  _GUICtrlComboBox_SetCurSel($cmbProfile, $SwitchtoProfile)
			cmbProfile()
			If _Sleep(2000) Then Return
			runBot()
   EndIf
   EndFunc
